# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170505000438) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.string   "number"
    t.integer  "account_id"
    t.boolean  "private"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["account_id"], name: "index_accounts_on_account_id", using: :btree
    t.index ["user_id"], name: "index_accounts_on_user_id", using: :btree
  end

  create_table "entities", force: :cascade do |t|
    t.string   "name"
    t.integer  "no"
    t.float    "frac"
    t.string   "unit"
    t.string   "street"
    t.string   "box"
    t.string   "city"
    t.string   "polsub"
    t.string   "postal"
    t.string   "nation"
    t.string   "phone"
    t.string   "email"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entries", force: :cascade do |t|
    t.integer  "transaktion_id"
    t.integer  "item_id"
    t.decimal  "price",          precision: 9, scale: 4
    t.float    "qty"
    t.integer  "account_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["account_id"], name: "index_entries_on_account_id", using: :btree
    t.index ["item_id"], name: "index_entries_on_item_id", using: :btree
    t.index ["transaktion_id"], name: "index_entries_on_transaktion_id", using: :btree
  end

  create_table "gendescs", force: :cascade do |t|
    t.string   "gendesc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "barcode"
    t.string   "brand"
    t.integer  "gendesc_id"
    t.float    "size"
    t.integer  "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gendesc_id"], name: "index_items_on_gendesc_id", using: :btree
    t.index ["unit_id"], name: "index_items_on_unit_id", using: :btree
  end

  create_table "transaktions", force: :cascade do |t|
    t.datetime "date"
    t.integer  "entity_id"
    t.boolean  "is_void"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["entity_id"], name: "index_transaktions_on_entity_id", using: :btree
    t.index ["user_id"], name: "index_transaktions_on_user_id", using: :btree
  end

  create_table "units", force: :cascade do |t|
    t.string   "unit"
    t.float    "factor"
    t.integer  "m"
    t.integer  "kg"
    t.integer  "s"
    t.integer  "a"
    t.integer  "k"
    t.integer  "cd"
    t.integer  "mol"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "accounts", "accounts"
  add_foreign_key "accounts", "users"
  add_foreign_key "entries", "accounts"
  add_foreign_key "entries", "items"
  add_foreign_key "entries", "transaktions"
  add_foreign_key "items", "gendescs"
  add_foreign_key "items", "units"
  add_foreign_key "transaktions", "entities"
  add_foreign_key "transaktions", "users"
end
