class AddUserRefToTransaktions < ActiveRecord::Migration[5.0]
  def change
    add_reference :transaktions, :user, foreign_key: true
  end
end
