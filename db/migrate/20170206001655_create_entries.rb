class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.references :transaktion, foreign_key: true
      t.references :item, foreign_key: true
      t.decimal :price, precision: 9, scale: 4
      t.float :qty
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
