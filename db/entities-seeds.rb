Entity.create(id: 1, name: "Kroger", no: 28250, unit: nil, street: "Dequindre", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-558-0064", email: nil, url: "https://www.kroger.com/")
Entity.create(id: 2, name: "Meijer #231", no: 28800, unit: nil, street: "Telegraph Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48034-1950", nation: "USA", phone: "248-304-9500", email: nil, url: "https://www.meijer.com/")
Entity.create(id: 3, name: "Seed Savers Exchange", no: 3074, unit: nil, street: "North Winn Rd.", box: nil, city: "Decorah", polsub: "IA", postal: "52101", nation: "USA", phone: "563-382-6104", email: nil, url: "http:www.seedsavers.org")
Entity.create(id: 5, name: "Far East Ginseng H&T", no: 33162, unit: nil, street: "Dequindre Rd.", box: nil, city: "Sterling Heights", polsub: "MI", postal: "48310", nation: "USA", phone: "586-977-0202", email: nil, url: "http://www.fareastginseng.com/")



Entity.create(id: 23, name: "Astounding Team", no: 2434, unit: nil, street: "Marlow", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: nil, email: "astoundingteam@gmail.com", url: "astoundingteam.com")


Entity.create(id: 32, name: "Meijer #222", no: 1005, unit: nil, street: "E. 13 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-307-4900", email: nil, url: "meijer.com")
Entity.create(id: 39, name: "Grainger", no: 25940, unit: nil, street: "Groesbeck Highway", box: nil, city: "Warren", polsub: "MI", postal: "48089-4144", nation: "USA", phone: "586-772-7790", email: nil, url: "www.grainger.com")
Entity.create(id: 40, name: "Petco", no: 28400, unit: nil, street: "Dequindre Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-751-3392", email: nil, url: "www.petco.com")
Entity.create(id: 41, name: "Produce Palace", no: 29300, unit: nil, street: "Dequindre Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-574-3000", email: nil, url: "www.ppifresh.com")
Entity.create(id: 42, name: "Aldi #78", no: 25851, unit: nil, street: "Grand River Ave.", box: nil, city: "Redford", polsub: "MI", postal: "48240", nation: "USA", phone: nil, email: nil, url: "www.aldi.us")
Entity.create(id: 43, name: "Social Security Administration", no: 6401, unit: nil, street: "Security Boulevard", box: nil, city: "Baltimore", polsub: "MD", postal: "21235", nation: "USA", phone: "410-965-1234", email: nil, url: "www.ssa.gov")

Entity.create(id: 45, name: "Anton Argires, Inc.", no: 12345, unit: nil, street: "S. Latrobe Ave.", box: nil, city: "Alsip", polsub: "IL", postal: "60803", nation: "USA", phone: "708-388-6250", email: "george@argires.com", url: "anuts.com")
Entity.create(id: 46, name: "Meijer #222 (gas station)", no: 1101, unit: nil, street: "E. 13 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-307-4929", email: nil, url: "meijer.com")
Entity.create(id: 47, name: "Neo Vision Eyewear, Inc.", no: 21201, unit: "#2226", street: "S. Elsner Rd.", box: nil, city: "Frankfort", polsub: "IL", postal: "60423", nation: "USA", phone: nil, email: "jizguerra@sbcglobal.net", url: "http://myworld.ebay.com/neovisioneyewear")



Entity.create(id: 51, name: "The Progressive Corporation", no: 6300, unit: nil, street: "Wilson Mills Rd.", box: "W33", city: "Mayfield Village", polsub: "OH", postal: "44143", nation: "USA", phone: "800-776-4737", email: nil, url: "www.progressive.com")
Entity.create(id: 52, name: "Ninawa Market", no: 29116, unit: nil, street: "Ryan Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-558-4550", email: nil, url: nil)
Entity.create(id: 53, name: "Miller Parking Services", no: 414, unit: nil, street: "Renaissance Dr. W.", box: nil, city: "Detroit", polsub: "MI", postal: "48243", nation: "USA", phone: "313-259-2434", email: "info@millerparking.com", url: "http://www.millerparking.com/")
Entity.create(id: 54, name: "Kelly's Sports Bar", no: 26615, unit: nil, street: "Ryan Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48091", nation: "USA", phone: "586-757-1250", email: nil, url: nil)

Entity.create(id: 56, name: "Nexcess.Net, LLC", no: 21700, unit: nil, street: "Melrose Ave.", box: nil, city: "Southfield", polsub: "MI", postal: "48075", nation: "USA", phone: "866-639-2377", email: "sales@nexcess.net", url: "www.nexcess.net")
Entity.create(id: 57, name: "US Post Office", no: 28401, unit: nil, street: "Mound Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48090-9998", nation: "USA", phone: nil, email: nil, url: nil)

Entity.create(id: 59, name: "Micro Center", no: 32800, unit: nil, street: "Concord Dr.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-291-8400", email: nil, url: "www.microcenter.com")
Entity.create(id: 60, name: "Meijer #237", no: 29505, unit: nil, street: "Mound Rd.", box: nil, city: "Warren", polsub: "Mi", postal: "48092", nation: "USA", phone: "1-586-573-2900", email: nil, url: "Meijer.com")
Entity.create(id: 61, name: "Monster Oil (Exxon station)", no: 1845, unit: nil, street: "E. 12 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: nil, email: nil, url: nil)
Entity.create(id: 62, name: "Eileen Lee", no: 19295, unit: nil, street: "Grandview", box: nil, city: "Detroit", polsub: "MI", postal: "48219", nation: "USA", phone: "313-720-7156", email: "withinsuch@gmail.com", url: nil)
Entity.create(id: 63, name: "Joe's Produce", no: 33152, unit: nil, street: "W. Seven Mile Rd.", box: nil, city: "Livonia", polsub: "MI", postal: "48152", nation: "USA", phone: "248-477-4333", email: nil, url: "www.joesproduce.com")

Entity.create(id: 65, name: "Monoprice, Inc.", no: 11707, unit: nil, street: "6th Street", box: nil, city: "Rancho Cucamonga", polsub: "CA", postal: "91730", nation: "USA", phone: "877.271.2592", email: "support@monoprice.com", url: "http://www.monoprice.com/")
Entity.create(id: 66, name: "BP station", no: 19100, unit: nil, street: "Telegraph Rd.", box: nil, city: "Detroit", polsub: "MI", postal: "48219", nation: "USA", phone: nil, email: nil, url: nil)
Entity.create(id: 67, name: "Pharmapacks", no: 110, unit: nil, street: "14th Ave.", box: nil, city: "College Point", polsub: "NY", postal: "11356", nation: "USA", phone: "855-797-2257", email: "customercare@pharmapacks.com", url: "http://www.pharmapacks.com/")

Entity.create(id: 69, name: "Amazon", no: 1200, unit: nil, street: "12th Ave. South", box: nil, city: "Seattle", polsub: "WA", postal: "98144", nation: "USA", phone: "206-266-1000", email: "info@amazon.com", url: "http://www.amazon.com/ref=nav_logo")

Entity.create(id: 71, name: "Costco", no: 999, unit: nil, street: "Lake Drive", box: nil, city: "Issaquah", polsub: "WA", postal: "98027", nation: "USA", phone: "425-313-8103", email: "info@costco.com", url: "http://www.costco.com/")
Entity.create(id: 72, name: "Shell", no: 30990, unit: nil, street: "Dequindre Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-575-9287", email: nil, url: nil)
Entity.create(id: 73, name: "Dentist.net", no: 2967, unit: "Suite G 110", street: "Michelson Dr", box: nil, city: "Irvine", polsub: "CA", postal: "95612", nation: "USA", phone: "877-224-4443", email: "customerservice@dentist.net", url: "http://www.dentist.net/")

Entity.create(id: 75, name: "Mobil", no: 31011, unit: nil, street: "Dequindre", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-583-2669", email: nil, url: nil)
Entity.create(id: 76, name: "Target", no: 28800, unit: nil, street: "Dequindre Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-353-1150", email: nil, url: nil)

Entity.create(id: 78, name: "Value World #8", no: 25646, unit: nil, street: "W. Eight Mile Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48075", nation: "USA", phone: "734-728-4610", email: nil, url: nil)
Entity.create(id: 79, name: "Target", no: 30020, unit: nil, street: "Grand River Ave.", box: nil, city: "Farmington Hills", polsub: "MI", postal: "48336", nation: "USA", phone: "248-476-1808", email: nil, url: "target.com")
Entity.create(id: 80, name: "Seven Mile Citgo LLC", no: 27350, unit: nil, street: "W. 7 Mile Rd.", box: nil, city: "Redford", polsub: "MI", postal: "48240", nation: "USA", phone: "313-766-4646", email: nil, url: nil)
Entity.create(id: 81, name: "Homestead Market - Div. of Hefty Creek Intl., LLC", no: 0, unit: nil, street: "Hefty Road", box: nil, city: "Monticello", polsub: "WI", postal: "53570", nation: "USA", phone: "608-527-4136", email: nil, url: "http://www.homesteadmarket.com/")

Entity.create(id: 83, name: "Mobile City Wireless", no: 3726, unit: nil, street: "Rochester Rd.", box: nil, city: "Troy", polsub: "MI", postal: "48083", nation: "USA", phone: "248-688-9983", email: nil, url: nil)
Entity.create(id: 84, name: "Sunoco", no: 26461, unit: nil, street: "Dequindre Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "734-620-8159", email: nil, url: "https://www.sunoco.com/locations/mi/26461-dequindre-road-madison-heights-mi-48071/")
Entity.create(id: 85, name: "Walgreens", no: 4010, unit: nil, street: "E. 13 Mile Rd.", box: nil, city: "Warren", polsub: "MI", postal: "48092", nation: "USA", phone: "586-575-9346", email: nil, url: "walgreens.com")
Entity.create(id: 86, name: "Drugstore.com, Inc.", no: 411, unit: "Suite 1600", street: "108th Ave. NE", box: nil, city: "Bellevue", polsub: "WA", postal: "98004", nation: "USA", phone: "425-372-4401", email: nil, url: "http://www.drugstore.com/")


Entity.create(id: 89, name: "Kroger", no: 33523, unit: nil, street: "W. Eight Mile Rd.", box: nil, city: "Livonia", polsub: "MI", postal: "48152", nation: "USA", phone: "248-471-6780", email: nil, url: "kroger.com")

Entity.create(id: 91, name: "BP", no: 22590, unit: nil, street: "Telegraph Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48075", nation: "USA", phone: "248-357-0980", email: nil, url: nil)

Entity.create(id: 93, name: "Monster Oil (Exxon)", no: 1845, unit: nil, street: "E. 12 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-336-9900", email: nil, url: nil)
Entity.create(id: 94, name: "Home Depot", no: 660, unit: nil, street: "W. 12 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-591-7520", email: nil, url: "http://www.homedepot.com/l/Madison-Heights/MI/Madison-Heights/48071/2731")
Entity.create(id: 95, name: "State of Michigan, Department of State", no: 25263, unit: nil, street: "Telegraph Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48033", nation: "USA", phone: "888-767-6424", email: nil, url: "https://services2.sos.state.mi.us/servicelocator/")
Entity.create(id: 96, name: "Downriver Refrigeration", no: 22131, unit: nil, street: "Van Dyke Ave.", box: nil, city: "Warren", polsub: "MI", postal: "48089", nation: "USA", phone: "586-755-7266", email: nil, url: nil)
Entity.create(id: 97, name: "Evergreen Supply Company", no: 20736, unit: nil, street: "Lahser Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48033", nation: "USA", phone: "248-354-8181", email: nil, url: nil)
Entity.create(id: 98, name: "Hilton Veterinary Clinic", no: 3250, unit: nil, street: "Hilton Rd.", box: nil, city: "Ferndale", polsub: "MI", postal: "48220", nation: "USA", phone: "248-955-3253", email: nil, url: "http://www.hiltonpetvet.com/")


Entity.create(id: 101, name: "Marathon", no: 19005, unit: nil, street: "Telegraph Rd.", box: nil, city: "Detroit", polsub: "MI", postal: "48219", nation: "USA", phone: nil, email: nil, url: nil)




Entity.create(id: 106, name: "Signal Group dba Solid Signal", no: 22285, unit: nil, street: "Roethel", box: nil, city: "Novi", polsub: "MI", postal: "48375", nation: "USA", phone: "877-312-4547", email: nil, url: "http://www.solidsignal.com/")
Entity.create(id: 107, name: "Telegraph Mobil", no: 22590, unit: nil, street: "Telegraph Rd.", box: nil, city: "Southfield", polsub: "MI", postal: "48033", nation: "USA", phone: "248-864-8136", email: nil, url: nil)
Entity.create(id: 108, name: "Young Again Products", no: 10207, unit: "#150", street: "310 N. Front St.", box: nil, city: "Wilmington", polsub: "NC", postal: "28401", nation: "USA", phone: "910-371-6775", email: "info@youngagain.com", url: "http://www.youngagain.com/magento")
Entity.create(id: 109, name: "Silver Stream LLC", no: 901, unit: nil, street: "W. Frank St.", box: nil, city: "Mitchell", polsub: "IN", postal: "47446", nation: "USA", phone: "812-849-6500", email: "sales@dishmasterfaucet.com", url: "https://dishmasterfaucet.com/")
Entity.create(id: 110, name: "Lowe's #1179", no: 2000, unit: nil, street: "Metropolitan Parkkway", box: nil, city: "Sterling Heights", polsub: "MI", postal: "48310", nation: "USA", phone: "586-446-4100", email: nil, url: "https://www.lowes.com/store/MI-Sterling-Height/1779")
Entity.create(id: 111, name: "Zerbo's", no: 34164, unit: nil, street: "Plymouth Rd.", box: nil, city: "Livonia", polsub: "MI", postal: "48150", nation: "USA", phone: "734-427-3144", email: nil, url: "http://www.zerbos.com/")
Entity.create(id: 112, name: "Parmenter's Northville Cider Mill", no: 714, unit: nil, street: "Base Line Rd.", box: nil, city: "Northville", polsub: "MI", postal: "48167", nation: "USA", phone: "248-349-3181", email: nil, url: "http://www.northvillecider.com/")
Entity.create(id: 113, name: "Shell", no: 24761, unit: nil, street: "W. 7 Mile Rd.", box: nil, city: "Detroit", polsub: "MI", postal: "48219", nation: "USA", phone: "313-535-3000", email: nil, url: nil)

Entity.create(id: 115, name: "sears.com", no: 3333, unit: nil, street: "Beverly Rd.", box: nil, city: "Hoffman Estates", polsub: "IL", postal: "60179", nation: "USA", phone: "847-286-2500", email: nil, url: "http://www.sears.com/")
Entity.create(id: 116, name: "Remer, Stanley, DO", no: 1385, unit: nil, street: "E. 12 Mile Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "248-336-0500", email: nil, url: nil)

Entity.create(id: 118, name: "Sunoco", no: 26461, unit: nil, street: "Dequindre Rd.", box: nil, city: "Madison Heights", polsub: "MI", postal: "48071", nation: "USA", phone: "734-620-8159", email: nil, url: "https://www.sunoco.com/locations/mi/26461-dequindre-road-madison-heights-mi-48071/")
