# README

Ettal is the successor of [Duper](https://github.com/n8chz/duper), and is a place for consumers to enter store receipts.
It is a work in progress, but people are welcome to contribute data at [https://ettal.herokuapp.com/transaktions/new](https://ettal.herokuapp.com/transaktions/new).

== Bugs ==

* Why is autofocus element not focusing in items/new?
* Fix edit buttons in transaktions/show

== TODO ==

* Provide a facility for merging gendesc's
* Fix problem of sometimes being unable to close dialogs
** Probably related to above: reload dialogs if dialog content fails to load
* Migrate to data model in which instance of transaktion and account belong to specific user accounts
** Write AccountsController#starter\_pack for same
