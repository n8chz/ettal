#!/bin/bash 

dropdb --if-exists ettal_development
dropdb --if-exists ettal_test
dropdb --if-exists ettal_production

dropuser --if-exists ettal

createuser -d ettal

sudo -u postgres psql <<< "alter user ettal password 'ettal';"

rails new ettal -d postgresql

cd ettal

sed -i 's/#username/username/g' config/database.yml
sed -i 's/#password:/password: ettal/g' config/database.yml
sed -i 's/#host/host/g' config/database.yml

# if migrate doesn't work see https://gist.github.com/ytbryan/4c4db2f12f2a06b698df

rake db:setup

rake db:migrate

rake db:setup

# If rails generate hangs see http://stackoverflow.com/a/31857620/948073


bundle config --delete bin    # Turn off Bundler's stub generator
rake rails:update:bin         # Use the new Rails 4 executables

bin/rails generate scaffold Entity name:string no:integer frac:float unit:string street:string box:string city:string polsub:string postal:string nation:string phone:string email:string url:string

bin/rails generate scaffold Transaktion date:datetime entity:references is_void:boolean

bin/rails generate scaffold Unit unit:string factor:float m:integer kg:integer s:integer a:integer k:integer cd:integer mol:integer

bin/rails generate scaffold Gendesc gendesc:string

bin/rails generate scaffold Item barcode:string brand:string gendesc:references size:float unit:references

#bin/rails generate scaffold User handle:string fullname:string pwdhash:string private:boolean

bin/rails generate scaffold Account name:string number:string account:references private:boolean

bin/rails generate scaffold Entry transaktion:references item:references price:decimal{9.4} qty:float account:references

bin/rake db:migrate

bin/rake routes > ./notes/routes.txt

# Now add devise:
# see https://github.com/plataformatec/devise

echo gem \'devise\' >> Gemfile

bundle install

rails generate devise:install


# see http://www.theunixschool.com/2012/06/insert-line-before-or-after-pattern.html
sed -i 's/^end$/  config.action_mailer.default_url_options = { host: "localhost", port: 3000 }\n&/' config/environments/development.rb

bin/rails generate devise User

rake db:migrate

bin/rails generate devise:views users # see https://github.com/plataformatec/devise

# Save the present script within the Rails project, for future reference:

cp ../ettal-mockup.sh ./notes

# NOTE:  Columns listed as :string should have been :string.
#        This was corrected in migration db/migrate/20151215142858_text_to_string.rb

