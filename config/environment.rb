# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# h/t jyli7 http://stackoverflow.com/q/8186584/948073

ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
   :tls => true,
   :address => "smtp.gmail.com",
   :port => 587,
   :domain => "gmail.com",
   :authentication => :login,
   :user_name => "[username]",
   :password => "[password]"
}
