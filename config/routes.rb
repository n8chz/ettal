Rails.application.routes.draw do
  #devise_for :users
  get 'welcome/index'
  get '/entries/new', to: redirect('/transaktions/new'), status: 200

  # Some controllers for JSON requests in support of interactivity:
  get '/entities/lookup', to: 'entities#lookup'
  get '/entities/latest', to: 'entities#latest'
  get '/accounts/lookup', to: 'accounts#lookup'
  get '/accounts/latest', to: 'accounts#latest'
  get '/items/lookup', to: 'items#lookup'
  get '/items/latest', to: 'items#latest'
  get '/gendescs/lookup', to: 'gendescs#lookup'
  get '/gendescs/latest', to: 'gendescs#latest'
  get '/units/lookup', to: 'units#lookup'
  get '/units/latest', to: 'units#latest'
  get '/items/account_for', to: "items#account_for"
  get '/accounts/starterpack', to: "accounts#starter_pack"
  post '/accounts/starterpack', to: "accounts#create_starter_pack"

  # Stuff to do after logging in:
  devise_for :users, :controllers => { :sessions => "sessions" }

  resources :entries
  resources :accounts
  resources :items
  resources :gendescs
  resources :units
  resources :transaktions
  resources :entities
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "welcome#index"
end
