# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


tweakAncestors = (element, newState) ->
  parentID = element.attr "data-parent"
  if typeof parentID != "undefined"
    selector = ".public[data-id='#{parentID}']"
    parent = ($ "#{newState}[data-id='#{parentID}']")
    parent.prop "checked", true # http://stackoverflow.com/a/18954842/948073
    tweakAncestors parent, newState


tweakDescendants = (element, newState) ->
  id = element.attr "data-id"
  # note that newState includes ".", so selector below is a class selector:
  selector = "#{newState}[data-parent='#{id}']"
  subAccounts = ($ "#{newState}[data-parent='#{id}']")
  subAccounts.each ->
      ($ this).prop "checked", true
      tweakDescendants ($ this), newState
  

# If we omit an account, we also omit all the sub-accounts all the way down.
# If we didn't, we'd create accounts that don't have parent accounts.
# If we mark an account public, we also mark parent accounts all the way up.
# If we mark one private, we mark ancestors as private if previously omitted,
# and descendants as private if previously public.

# Due to debugging nightmare, for now we'll settle for a safe (if unconvenient)
# practice of propagating priave marks both up and down the chain,
# regardless of previous state
($ document).on "turbolinks:load", ->
  ($ ".omit").click ->
    tweakDescendants ($ this), ".omit"
  ($ ".private").click ->
    tweakAncestors ($ this), ".private"
    tweakDescendants ($ this), ".private"
  ($ ".public").click ->
    tweakAncestors ($ this), ".public"

