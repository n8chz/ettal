# set up dialog iframe for button, in case a new ActiveRecord is needed:

window.setupAutocomplete = (labelTarget, valueTarget) ->
  labelTarget.autocomplete
    source: labelTarget.data "lookup"
    select: (event, ui) ->
      label = ui.item.label
      value = ui.item.value
      labelTarget.blur ->
        labelTarget.val label
        valueTarget.val value

window.setupDialog = (iframe, button, model, labelTarget, valueTarget) ->
  latest_url = button.data "latest"
  dialog = iframe.dialog
    autoOpen: false
    modal: true
    # Don't understand why simply using window props
    # doesn't work for nested dialog
    width: Math.max 0.9*window.innerWidth, 640
    height: Math.max 0.9*window.innerHeight, 480
    close: (event, ui) ->
      $.get latest_url, null, (latest) ->
        valueTarget.val latest.value
        labelTarget.val latest.label
  button.click ->
    dialog.dialog "open"
    # iframe.src += "" # h/t Daniel Hug http://jsfiddle.net/Daniel_Hug/dWm5k/
    # h/t user1570636 http://stackoverflow.com/a/28996801/948073
    contents = iframe.contents()
    # nav bar not needed in modal popups:
    contents.find("nav").remove()
    # make autofocus element actually focus
    contents.find("[autofocus]").focus()
    form = contents.find "form"
    # h/t Bruce http://stackoverflow.com/a/34712852/948073
    form.submit -> (iframe.load -> dialog.dialog "close")

window.autocompleteSpan = (span) ->
  model = span.attr "data-model"
  valueTarget = span.find ".value"
  labelTarget = span.find ".label"
  plus = span.find ".plus"
  # set up spans to receive data once jquery-ui autocomplete completes:
  window.setupAutocomplete labelTarget, valueTarget
  # set up dialog iframe for button, in case a new ActiveRecord is needed:
  source = span.attr "data-source"
  iframe = ($ "<iframe>", {src: source})
  window.setupDialog iframe, plus, model, labelTarget, valueTarget

# in-form calculator for price and quantity fields
window.arithmEval = (exp) ->
  # remove any whitespace characters:
  exp = exp.split(/\s/).join("")
  # verify that only legal characters remain:
  if exp.match /^[0-9+\-*\/().]+$/
    # consider the string sanitized, use eval function:
    eval exp
  else
    # something weird has been included
    ""

window.nearestCent = (x) ->
  Math.floor(100*x+0.5)/100

