# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# form is valid iff
## debit and credit blocks each contain at least one entry
## total debits == total credits
## every entry has an account_id
## TODO: entity_id should also be filled in

# submit button is greyed out until form is valid
# submit button state is handled by this function

formValidation = (tally, missingAccounts) ->
  submit = ($ "#post")
  if window.nearestCent(tally) == 0 && missingAccounts == 0 && ($ ".account_id.value[value]").length > 1
    # h/t basic6 http://stackoverflow.com/a/26035133/948073
    submit.prop "disabled", false
  else
    submit.prop "disabled", true

numericBlur = ->
  ($ this).val arithmEval ($ this).val()
  tally = totalDebits = totalCredits = 0
  missingAccounts = 0
  ($ ".replicant").each ->
    index = ($ this).attr "data-index"
    column = ($ this).attr("data-column")
    price = Number ($ this).find("input.price").val()
    # price = -price if column == "1"
    # qty = if column == "0" then ($ this).find("input.qty").val() else 1
    qty = if column == "0" then Number ($ this).find("input.qty").val() else 1
    pq = window.nearestCent(price*qty)
    if pq
      if column == "0"
        # ($ "#debits").val nearestCent(Number(($ "#debits").val())+pq)
        totalDebits += pq
        ($ this).find("input.qty").val()
      else
        # ($ "#credits").val nearestCent(Number(($ "#credits").val())+pq)
        totalCredits += pq
        price = -price
        pq = -pq
      accountField = ($ this).find("input.account_id")
      accountId = accountField.val()
      if accountId == ""
        missingAccounts++
        accountField.addClass("red")
        accountField.focus()
      else
        accountField.removeClass("red")
        ($ "#debits").val window.nearestCent totalDebits
        ($ "#credits").val window.nearestCent totalCredits
        tally += pq
        ($ "#difference").val window.nearestCent tally
        ($ this).find("input.amount").val pq
        formValidation tally, missingAccounts

$(document).on "turbolinks:load", ->


  # start w. submit button disabled, pending form validation
  ($ "#post").prop "disabled", true

  # start w. running totals at 0
  ($ "#debits").val "0.00"
  ($ "#credits").val "0.00"
  ($ "#difference").val "0.00"

  # add jquery-ui datepicker to date input
  ($ "#transaktion_date").datepicker
    dateFormat: "yy-mm-dd"

  # start with focus on first input element
  ($ "#transaktion_date").focus()

  ($ ".autocomplete").each ->
    window.autocompleteSpan ($ this)

  ($ ".replicator").click ->
    entryBlock = ($ this).closest ".entry-block"
    lastEntry = entryBlock.find(".replicant").last()
    newEntry = lastEntry.clone()
    oldIndex = newEntry.data "index"
    newIndex = oldIndex+2
    newEntry.attr "data-index", newIndex
    # make sure name attributes also reflect the new index
    namedFields = newEntry.find "[name*=\"#{oldIndex}\"]"
    namedFields.each ->
      name =($ this).attr "name"
      ($ this).attr "name", name.replace oldIndex, newIndex
    idFields = newEntry.find "[id*=\"_#{oldIndex}_\"]"
    idFields.each ->
      id =($ this).attr "id"
      ($ this).attr "id", id.replace oldIndex, newIndex
    newEntry.find(".autocomplete").each ->
      window.autocompleteSpan ($ this)
    newEntry.find("input").val null
    newEntry.find(".numeric").blur numericBlur
    newEntry.insertAfter(lastEntry)
    # ergonomics
    newEntry.find(".qty").val 1
    newEntry.find(".item_id.label").focus()

  ($ ".numeric").blur numericBlur

  ($ ".edit-in-dialog").each ->
    src = ($ this).attr "data-source"
    iframe = ($ "<iframe>").attr "src", src

    dialog = iframe.dialog
      autoOpen: false
      modal: true
      # Don't understand why simply using window props
      # doesn't work for nested dialog
      width: Math.max 0.9*window.innerWidth, 640
      height: Math.max 0.9*window.innerHeight, 480
      #close: (event, ui) ->
        ##TODO refresh current page
        #console.log "foo"
    ($ this).click ->
      dialog.dialog "open"
      # iframe.src += "" # h/t Daniel Hug http://jsfiddle.net/Daniel_Hug/dWm5k/
      # h/t user1570636 http://stackoverflow.com/a/28996801/948073
      contents = iframe.contents()
      # nav bar not needed in modal popups:
      contents.find("nav").remove()
      form = contents.find "form"
      # h/t Bruce http://stackoverflow.com/a/34712852/948073
      form.submit -> (iframe.load -> dialog.dialog "close")

  # Once an item is entered, we'd like to know what account it may have been
  # charged to in prior transactions. Then, set the account field to that
  # account for starters, with the option of changing that.
  ($ ".item_id.label").blur ->
    #TODO pull up relavant account, if there is one
    item_id = ($ this).closest(".autocomplete").find(".value").val()
    url = "/items/account_for.json?item_id=#{item_id}"
    valueTarget = ($ this).closest(".replicant").find(".account_id.value")
    labelTarget = ($ this).closest(".replicant").find(".account_id.label")
    $.get url, null, (account) ->
      valueTarget.val account.value
      labelTarget.val account.label


