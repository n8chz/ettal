# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Size may require calculation, as in package include six 12-ounce cans, etc.
$(document).on "turbolinks:load", ->
  ($ ".size").blur ->
    ($ this).val window.arithmEval ($ this).val()

