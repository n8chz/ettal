module AccountsHelper

  def name_and_balance account
    friendly_name = account.friendly_name
    path = friendly_name.split "|"
    if path.length > 0
      name = escape_once("&nbsp;"*(2*(path.length-1))+path.last)
      balance = account.balance
      balance = -balance unless friendly_name.start_with? "assets"
      content_tag :tr do
        concat content_tag :td, link_to(name, account_path(account), :class => "no-underline")
        concat content_tag :td, "%.2f" % balance, :class => "amount"
      end
    else
      p "nameless account?"
      p account.id
    end
  end

  def balance_sheet_colgroup
    content_tag :colgroup do
      ["acct_name", "acct_bal"].each do |class_name|
        concat content_tag :col, nil, :class => class_name
      end
    end
  end

  def balance_sheet_column groups, column
    if column == 0 # debit
      keys = groups.keys.select { |key| key == "assets" }
    else # credit
      keys = groups.keys.reject { |key| key == "assets" }
    end
    list = ActiveSupport::SafeBuffer.new
    column_total = 0
    keys.each do |key|
      accounts = groups[key]
      accounts.sort_by! {|account| account.friendly_name}
      accounts.each do |account|
        list << (name_and_balance account)
        column_total += account.balance * (1-2*column)
      end
    end
    tbody = content_tag :tbody, list
    foot_row = content_tag :tr do
      concat content_tag :td
      concat content_tag :td, "%.2f" % column_total, :class => ["amount", "column-total"]
    end
    content_tag :td, :class => "top-align" do
      content_tag :table do
        concat balance_sheet_colgroup
        concat tbody
        concat content_tag :tfoot, foot_row
      end
    end
  end

  def starter_pack_helper root_id
    if root_id != nil
      account = Account.find(root_id)
      parent = account.account_id
      friendly = account.friendly_name
      family = friendly.split("|").shift
      radios = ["omit", "public", "private"].collect do |selection|
        name = "account[#{friendly}]"
        checked = false
        checked = true if family == "expenses" && selection == "public"
        checked = true if family != "expenses" && selection == "private"
        (radio_button_tag name, selection, checked, \
          class: selection, data: {parent: parent, id: root_id}) \
        << \
        (label_tag "#{name}_#{selection}", selection)
      end
      content = content_tag :div do
        content_tag :strong, friendly
      end
      content << (content_tag :div, radios.reduce {|x, y| x << y})
    else
      content = ""
    end
    content_tag :fieldset, class: "select-account" do
      concat content
      Account.where(account_id: root_id, user_id: nil).each do |account|
        concat starter_pack_helper account.id
      end
    end
  end

end

