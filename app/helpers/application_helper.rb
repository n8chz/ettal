module ApplicationHelper

  def label_and_input form, property, caption, tabindex, attributes = {}
    layout = form.label property, caption
    layout << (form.text_field property, tabindex: tabindex, :autofocus => attributes[:autofocus], :class => attributes[:class])
  end

  def name_attr model_str, index, fkey_str
    name = "#{model_str}" << (index ? "[#{index}]" : "") << "[#{fkey_str}]"
  end

  # misc is a hash arg
  # misc[:index] holds an index,
  # for indexed form submissions (name attr)
  # such as that used in entries in transaktion/new
  # misc[:label] and misc[:value]
  # are for use in /*/edit views,
  # so current (pre-edit) values can be displayed
  def autofill_inputs model, fkey, caption, misc = {}
    model_str = model.to_s
    fkey_str = fkey.to_s
    fkey_pl = fkey_str.chomp("_id").pluralize
    p misc
    index = misc[:index]
    indexer = index != nil ? "_#{index}_" : "_"
    id = "#{model_str}#{indexer}#{fkey_str}"
    name = name_attr model_str, index, fkey_str
    lookup_url = "/#{fkey_pl}/lookup.json"
    latest_url = "/#{fkey_pl}/latest.json"
    source_url = "/#{fkey_pl}/new"
    # hidden input for form data (value)
    layout = (tag :input, name: name, id: id, class: [fkey, "value"], type: "hidden", value: misc[:value])
    # label tag
    layout = (content_tag :label, caption, for: id) << layout if caption
    # visible input for friendly name (label)
    layout << (tag :input, class: [fkey, "label"], data: {lookup: lookup_url}, value: misc[:label])
    # button for creating new instance for fkey field if necessary
    layout << (button_tag "+", data: {source: source_url, latest: latest_url}, class: [fkey, "plus"], type: "button")
    content_tag :span, layout, class: ["autocomplete"], data: {model: fkey, source: source_url} 
  end

  def new_indexed_input model, index, attr
    is_fkey = attr.end_with?("_id") # but see http://stackoverflow.com/a/42564311/948073
    if is_fkey
      autofill_inputs model, attr, nil, :index => index
    else
      name = name_attr model.to_s, index, attr
      tag :input, name: name, class: ["numeric", attr]
    end
  end

  def new_td model, index, attr
    content_tag :td, (new_indexed_input model, index, attr)
  end

  def transaktion_entry index, column
    # column is 0 for debit 1 for credit
    if column == 1
      attrs = ["account_id", "price"]
    else
      attrs = ["item_id", "account_id", "price", "qty"]
    end
    content_tag :tr, data: {index: index, column: column}, class: ["replicant"] do
      attrs.each do |attr|
	concat new_td :entry, index, attr
      end
      if column == 0
        amount = tag :input, disabled: true, class: ["amount"]
        concat content_tag :td, amount
      end
    end
  end

end
