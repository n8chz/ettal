module EntitiesHelper

  def civilize str
    str.strip.gsub /\s{2,}/, " "
  end

  def conditional_line str
    if str && str != ""
      concat str
      concat tag :br
    end
  end

  def entity_prettyprint entity
=begin
    concat content_tag :div do
      concat content_tag :p do
        concat entity.name
        add1 = [entity.no, entity.street, entity.frac].join " "
        add1.gsub!(/\s+/, " ").strip!
        add2 = entity.unit
        add3 = entity.box
        add4 = "#{entity.city}, #{entity.polsub} #{entity.postal}"
        add5 = entity.nation
        address = [add1, add2, add3, add4, add5].select {|a| a.to_s != ""}
        p address
        address.each do |line|
          concat tag :br
          concat line
        end
      end
      concat content_tag :p do
        contact_info = [entity.phone, entity.email, entity.url]
        contact_info.select! {|a| a.to_s != ""}
        if contact_info.length > 0
          concat contact_info[0]
          contact_info[1..-1].each do |line|
            concat tag :br
            concat line
          end
        end
      end
    end
=end
  end

end
