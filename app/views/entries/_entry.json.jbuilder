json.extract! entry, :id, :transaktion_id, :item_id, :price, :qty, :account_id, :created_at, :updated_at
json.url entry_url(entry, format: :json)