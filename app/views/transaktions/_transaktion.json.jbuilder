json.extract! transaktion, :id, :date, :entity_id, :is_void, :created_at, :updated_at
json.url transaktion_url(transaktion, format: :json)