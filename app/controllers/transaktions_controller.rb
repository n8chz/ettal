class TransaktionsController < ApplicationController
  before_action :set_transaktion, only: [:show, :edit, :update, :destroy]

  # GET /transaktions
  # GET /transaktions.json
  def index
    @transaktions = Transaktion.where(user_id: current_user_id)
  end

  # GET /transaktions/1
  # GET /transaktions/1.json
  def show
  end

  # GET /transaktions/new
  def new
    @transaktion = Transaktion.new
  end

  # GET /transaktions/1/edit
  def edit
  end

  # POST /transaktions
  # POST /transaktions.json
  def create
    @transaktion = Transaktion.new(transaktion_params)

    respond_to do |format|
      if valid_transaktion? and @transaktion.save
        @transaktion.user_id = current_user_id
        transaktion_id = @transaktion.id
        params["entry"].each_pair do |key|
          entry_params = params["entry"][key]
          entry_params[:transaktion_id] = transaktion_id
          # TODO make sure item_id is not a required field in model
          if key.to_i % 2 == 1 # odd key, credit entry_params
            entry_params["qty"] = -1
          end
          entry_params.permit!
          entry = Entry.new(entry_params)
        end
        format.html { redirect_to @transaktion, notice: 'Transaktion was successfully created.' }
        format.json { render :show, status: :created, location: @transaktion }
      else
        format.html { render :new }
        format.json { render json: @transaktion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transaktions/1
  # PATCH/PUT /transaktions/1.json
  def update
    respond_to do |format|
      if @transaktion.update(transaktion_params)
        format.html { redirect_to @transaktion, notice: 'Transaktion was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaktion }
      else
        format.html { render :edit }
        format.json { render json: @transaktion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transaktions/1
  # DELETE /transaktions/1.json
  def destroy
    @transaktion.destroy
    respond_to do |format|
      format.html { redirect_to transaktions_url, notice: 'Transaktion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaktion
      @transaktion = Transaktion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaktion_params
      params.require(:transaktion).permit(:date, :entity_id, :is_void)
    end

    def valid_transaktion?
      #TODO:
      # make sure all accounts referenced are accessible to user
      # make sure the transaktion entries balance
      # make sure data and entity fields are filled in

      # for now:
      true
    end

end
