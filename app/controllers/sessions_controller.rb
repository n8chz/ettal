class SessionsController < Devise::SessionsController
# Had to comment out these things to stop getting DoubleRenderError
  # before_filter :before_login, :only => :create
  after_action :after_login, :only => :create
  after_action :after_logout, :only => :destroy

  def before_login
  end

  def after_logout
    ApplicationRecord.current_user_id = user_signed_in? && current_user.id
  end

  def after_login
  end

  def after_sign_in_path_for(resource)
    ApplicationRecord.current_user_id = user_signed_in? && current_user.id
    if Account.friendly_names.length > 0
      super
    else
      accounts_starterpack_path
    end
  end

end

