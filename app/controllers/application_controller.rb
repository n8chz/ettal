class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  # h/t RSB http://stackoverflow.com/a/28804809/948073
  before_filter :set_current_user

  def set_current_user
    Account.current_user = current_user
    Account.user_signed_in = user_signed_in?
  end

  def model_class
    # h/t Joshua Cheek http://stackoverflow.com/a/3163899/948073
    self.class.to_s.chomp("Controller").singularize.constantize
  end

  def lookup
    friendly_names = self.model_class.friendly_names
    p "about to enter matching_names loop"
    matching_names = friendly_names.select do |name|
      p name
      # TODO: figure out what's going wrong, example
      # TypeError (no implicit conversion of Symbol into Integer):
      name[:label].downcase.index(params[:term])
    end
    respond_to do |format|
      format.json { render json: matching_names, status: :ok }
    end
  end

  def latest
    # model_class = self.class.to_s.chomp("Controller").singularize.constantize
    latest_thing = model_class.order(id: :desc).limit(1).collect { |thing|
     {value: thing.id, label: thing.friendly_name}
    }
    respond_to do |format|
      if latest_thing
        format.json { render json: latest_thing[0]}
      else
        format.json { nil }
      end
    end
  end

  def current_user_id
    user_signed_in? ? current_user.id : nil
  end

end

