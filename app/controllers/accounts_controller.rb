class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.where(user_id: ApplicationRecord.current_user_id)
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)
    Rails.logger.info(@account.errors.inspect)
    # see http://stackoverflow.com/a/38633712/948073

    respond_to do |format|
      if @account.name && @account.name.length
        @account.user_id = ApplicationRecord.current_user_id
        if @account.save
          Account.update_friendly_names @account
          format.html { redirect_to @account, notice: 'Account was successfully created.' }
          format.json { render :show, status: :created, location: @account }
        else
          p @account.errors
          format.html { render :new }
          format.json { render json: @account.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    if @account.user_id = ApplicationRecord.current_user_id
      respond_to do |format|
        if @account.update(account_params)
          format.html { redirect_to @account, notice: 'Account was successfully updated.' }
          format.json { render :show, status: :ok, location: @account }
        else
          format.html { render :edit }
          format.json { render json: @account.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    if @account.user_id = ApplicationRecord.current_user_id
      @account.destroy
      respond_to do |format|
        format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  # GET /accounts/starterpack
  def starter_pack
    respond_to do |format|
      format.html { render accounts_starterpack_path }
    end
  end

  # POST /accounts/starterpack
  def create_starter_pack
    switches = params[:account].to_unsafe_h
    switches.reject! { |fn, op| op == "omit" }
    # sort by friendly name to guarantee that no account will be processed
    # before its parent account, so each can reference its parent (if any)
    switches.sort_by { |fn, op| fn }
    # map is a hash that maps parent account names to their own account numbers
    # this is so we know what number to supply the account_id attribute
    # when staging new account for save
    map = {}
    user_id = params[:user_id]
    # make sure signed in user is same now as when the form was input
    if user_signed_in? && current_user.id.to_s == user_id
      switches.each do |fn, op|
        levels = fn.split "|"
        name = levels.pop
        parent = levels.join "|"
        parent_id = map[parent]
          new_acct = Account.new \
            name: name, private: op == "private", account_id: parent_id, user_id: user_id
          if new_acct.save
            # save id of new account so sub-accounts can reference it
            map[fn] = new_acct.id
          end
      end
    end
    respond_to do |format|
      format.html { redirect_to accounts_url }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:name, :number, :account_id, :private)
    end
end
