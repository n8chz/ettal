class Transaktion < ApplicationRecord
  belongs_to :entity

  # Magnitude is defined (WLOG) as your choice of sum of credit entries or
  # sum of debit entries
  def magnitude
    Entry.where(transaktion_id: self.id, qty: -1).sum :price
  end

end
