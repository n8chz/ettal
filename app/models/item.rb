class Item < ApplicationRecord
  belongs_to :gendesc
  belongs_to :unit, optional: true
end
