class Account < ApplicationRecord
  # h/t Igor_Marques http://stackoverflow.com/a/38986580/948073
  belongs_to :account, optional: true

  # h/t RSB http://stackoverflow.com/a/28804809/948073
  mattr_accessor :current_user
  mattr_accessor :user_signed_in

  # Get the full pathname of an account
  def account_pathname
    account_fullname = self.name
    account_level = self
    loop do
      break if not account_level.account_id
      account_level = Account.find(account_level.account_id)
      break if account_level == self # closed loop detection TODO make it a DB constraint
      account_fullname = "#{account_level.name}|#{account_fullname}"
    end
    account_fullname
  end

  def balance
    bal = 0
    Entry.where(:account_id => self.id).each do |x|
      bal += x.price*x.qty
    end
    bal
  end

  def self.reset_friendly_names
    p "reset_friendly_names"
    # id of logged in user if there is one, or else nil
    user_id = user_signed_in && current_user.id
    p "user_id: #{user_id}"
    @friendly_names = Account.where(user_id: user_id).collect do |account|
      {label: account.friendly_name, value: account.id}
    end
  end

end
