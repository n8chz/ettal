class ApplicationRecord < ActiveRecord::Base
  include Devise::Controllers::Helpers # h/t everplays http://stackoverflow.com/a/17126639/948073
  self.abstract_class = true
  cattr_accessor :current_user_id

  def friendly_name
    case self.class.to_s
      when "Entity"
        "#{self.name} (#{self.city}, #{self.polsub})"
      when "Account"
        self.account_pathname
      when "Item"
        # make sure this is correct
        gendesc = Gendesc.find(self.gendesc_id).gendesc
        "#{self.brand} #{gendesc} #{self.size} #{self.unit.short_form if self.unit}" << " (#{self.barcode})" if self.barcode
      when "Gendesc"
        self.gendesc
      when "Unit"
        self.unit
      else
        "???"
    end
  end

  def self.friendly_names
    if self == Account
      records = self.where(user_id: self.current_user_id)
    else
      records = self.all
    end
    @friendly_names = records.collect do |instance|
      {label: instance.friendly_name, value: instance.id}
    end
  end

# "manually caching" version of self.friendly_names:
=begin
  # make sure class variable @friendly_names is defined
  def self.friendly_names
    if !(defined? @friendly_names) || @friendly_names == [] # kludge
      @friendly_names = self.all.collect do |instance|
        # following line is to clean up after previous mistakes
	# max_val = instance[:value] if instance[:value] > max_val
	# {label: instance.friendly_name, value: instance[:id]}
        # max_val = instance.id if instance.id > max_val
        {label: instance.friendly_name, value: instance.id}
      end 
    end
    @friendly_names
  end
=end

  # TODO add ! to method name
  def self.update_friendly_names record
    @friendly_names ||= self.friendly_names
    @friendly_names.push({label: record.friendly_name, value: record.id})
    @friendly_names.sort! do |x, y|
      x["value"] <=> y["value"]
    end
    # this is probably redundant
    @friendly_names
  end

end
