class Unit < ApplicationRecord

  def short_form
    self.friendly_name.sub(/\(.*$/, "").strip.singularize
  end

end
