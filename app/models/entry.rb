class Entry < ApplicationRecord
  belongs_to :transaktion
  belongs_to :item, optional: true
  belongs_to :account
end
